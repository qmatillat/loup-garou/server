use std::convert::TryInto;

use crate::{
    consts::{PlayerID, RoleID, TurnID},
    message,
    playerdb::PlayerDb,
    roles::trait_roles::{Event, MessageType, RoleManager, TurnInfo},
    RoleDatabase,
};

/// An executor that will run a game
pub struct Executor {
    /// Turn list (in playing order)
    turn: Vec<Box<dyn RoleManager>>,
    /// Player database
    player_db: PlayerDb,

    /// Current turn step
    turn_step: usize,
    /// Current turn id
    turn_id: TurnID,

    /// Temporary message buffer
    message_queue: Vec<(MessageType, message::Send)>,
}

impl Executor {
    /// Create a new `Executor` based on player's name and a role database
    #[must_use]
    pub fn new(player_name: Vec<String>, roledb: &RoleDatabase) -> Self {
        let mut number_role = roledb.select(
            player_name
                .len()
                .try_into()
                .expect("More player that can fit in PlayerNb"),
        );

        number_role.sort_unstable_by_key(|&(_, r)| r.priority());

        let turn: Vec<_> = number_role
            .iter()
            .map(|&(nb, role)| role.create_manager(nb))
            .collect();

        let player_db = PlayerDb::create(player_name, number_role);

        let mut exec = Self {
            turn,
            player_db,

            turn_step: 0,
            turn_id: 0,

            message_queue: Vec::with_capacity(2),
        };

        exec.start_turn();

        exec
    }

    /// Return the current turn
    #[must_use]
    fn turn(&self) -> &dyn RoleManager {
        self.turn
            .get(self.turn_step)
            .expect("turn_step should always be valid")
            .as_ref()
    }

    /// Handle an event
    fn send_message(&mut self, event: Event) {
        println!("{:?}", event);
        let player_db = &mut self.player_db;
        let turn = self
            .turn
            .get_mut(self.turn_step)
            .expect("turn_step should always be valid")
            .as_mut();
        match turn.handle_event(event, player_db, &mut self.message_queue) {
            TurnInfo::Ok => (),
            TurnInfo::TurnDone => self.next_turn(),
        }
    }

    /// End current trun and start next one
    fn next_turn(&mut self) {
        let old = self.turn_info();
        self.message_queue
            .push((MessageType::Broadcast, message::Send::EndTurn));

        self.turn_step += 1;
        if self.turn_step >= self.turn.len() {
            self.turn_id += 1;
            self.turn_step = 0;
        }

        let new = self.turn_info();

        for t in &mut self.turn {
            t.handle_event(
                Event::OnBeetweenStep(old, new),
                &mut self.player_db,
                &mut self.message_queue,
            );
        }

        self.start_turn();
    }

    /// Start a turn
    fn start_turn(&mut self) {
        if self.turn_step == 0 {
            self.player_db.end_of_turn();
        }
        let playing = self
            .player_db
            .list()
            .filter_map(|(id, player)| {
                if self.turn().should_play_at_this_turn(player, self.turn_id) {
                    Some(id)
                } else {
                    None
                }
            })
            .collect();
        self.send_message(Event::StartTurn(playing, self.turn_id))
    }

    /// Return the player database
    #[must_use]
    pub const fn player_db(&self) -> &PlayerDb {
        &self.player_db
    }

    /// Handle a message sent by a client
    pub fn handle_event(&mut self, from_player: PlayerID, msg: message::Recv) {
        self.send_message(Event::MsgPlayer(from_player, msg));
    }

    /// Return the name of current turn
    #[must_use]
    pub fn turn_info(&self) -> RoleID {
        self.turn().name()
    }

    /// Clear and return the list of all pending message
    #[must_use]
    pub fn get_message(&mut self) -> std::vec::Drain<(MessageType, message::Send)> {
        self.message_queue.drain(..)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        base_roles::{cupidon::CupidonInfo, loups::LoupInfo, villager::VillagerInfo},
        role_database::tests::roledb,
        roles::RawRoleInfo,
    };

    #[test]
    fn basic_exec() {
        let player = vec![
            "PlayerA".to_string(),
            "PlayerB".to_string(),
            "PlayerC".to_string(),
            "PlayerD".to_string(),
        ];

        let mut exec = Executor::new(player, &roledb());

        let loup_id = {
            let mut loup = exec.player_db().get_of_role(LoupInfo::NAME);
            let (loup_id, _) = loup.next().expect("At least one Wolf");
            loup_id
        };

        let cupidon_id = exec
            .player_db()
            .get_of_role(CupidonInfo::NAME)
            .next()
            .expect("At least one Cupidon")
            .0;

        let villager = exec
            .player_db()
            .get_of_main_role(VillagerInfo::NAME)
            .map(|(id, _)| id)
            .collect::<Vec<_>>();

        assert_eq!(exec.turn_info(), CupidonInfo::NAME);

        exec.player_db().print();

        exec.handle_event(
            cupidon_id,
            message::Recv::VoteFor {
                player_id: Some(villager[0]),
                kind: 0,
            },
        );
        exec.handle_event(
            cupidon_id,
            message::Recv::VoteFor {
                player_id: Some(villager[1]),
                kind: 1,
            },
        );

        exec.handle_event(cupidon_id, message::Recv::VoteDone(true));

        assert!(matches!(
            exec.get_message().last(),
            Some((_, message::Send::EndTurn))
        ));

        assert_eq!(exec.turn_info(), LoupInfo::NAME);

        // Check that non wolf player can't play
        for i in exec.player_db.ids() {
            if i == loup_id {
                continue;
            }

            exec.handle_event(
                i,
                message::Recv::VoteFor {
                    player_id: Some(cupidon_id),
                    kind: 1,
                },
            );
        }

        for (_, m) in exec.get_message() {
            assert!(matches!(m, message::Send::Error(_)))
        }

        exec.handle_event(
            loup_id,
            message::Recv::VoteFor {
                player_id: Some(cupidon_id),
                kind: 1,
            },
        );

        assert!(matches!(
            exec.get_message().last(),
            Some((_, message::Send::Selected(_)))
        ));
        assert_eq!(exec.turn_info(), LoupInfo::NAME);

        exec.handle_event(loup_id, message::Recv::VoteDone(true));
        assert!(matches!(
            exec.get_message().last(),
            Some((_, message::Send::EndTurn))
        ));

        assert_eq!(exec.turn_info(), VillagerInfo::NAME);
        assert!(exec.player_db().player_info(cupidon_id).unwrap().dead());

        exec.player_db().print();
        for i in exec.player_db().ids() {
            exec.handle_event(
                i,
                message::Recv::VoteFor {
                    player_id: Some(villager[0]),
                    kind: 1,
                },
            );

            let (_, msg) = exec.get_message().last().unwrap();
            match msg {
                message::Send::Selected(_) if i != cupidon_id => (),
                message::Send::Error(_) if i == cupidon_id => continue,
                other => panic!(
                    "Unexpected message for player {}  != {}: {:?}",
                    i, cupidon_id, other
                ),
            }
        }

        for i in exec.player_db().ids() {
            exec.handle_event(i, message::Recv::VoteDone(true));
            let (_, msg) = exec.get_message().last().unwrap();
            match msg {
                message::Send::Error(_) if i == cupidon_id => (),
                message::Send::Selected(_) => (),
                message::Send::EndTurn => break,
                other => panic!("Unexpected message for player {}: {:?}", i, other),
            }
        }

        assert_eq!(exec.turn_info(), LoupInfo::NAME);

        exec.player_db().print();

        assert!(exec.player_db().player_info(villager[0]).unwrap().dead());
        assert!(exec.player_db().player_info(villager[1]).unwrap().dead());
    }
}
