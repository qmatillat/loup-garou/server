use std::fmt::Debug;

use crate::{
    consts::{AlternateRole, PlayerNb, RoleID, TurnID},
    message,
    playerdb::{Player, PlayerDb},
};

use super::{
    trait_roles::{Event, MessageType, RoleInfo, RoleManager, TurnInfo},
    NumberInfo,
};

/// Helper to define statics information about a role
pub trait RawRoleInfo {
    /// Name of this role. Must be unique
    const NAME: RoleID;

    /// Weight info for this role
    const WEIGTH: NumberInfo;

    /// Priority of this role (used to determine turn order)
    const PRIORITY: u8;

    /// List of alternate role
    ///
    /// Considered as if all player also had those roles
    /// Ex: Cupidon act as if he is a Villager (play and win with them), `WhiteWolf` act as if `Wolf`
    const ALTERNATE_ROLE: AlternateRole;

    /// Create the manager associated with this role
    fn create_manager(&self, num_player: PlayerNb) -> Box<dyn RoleManager>;
}

impl<Raw: RawRoleInfo> RoleInfo for Raw {
    fn default_weight(&self) -> NumberInfo {
        Self::WEIGTH
    }

    fn name(&self) -> RoleID {
        Self::NAME
    }

    fn priority(&self) -> u8 {
        Self::PRIORITY
    }

    fn alternate_role(&self) -> AlternateRole {
        Self::ALTERNATE_ROLE
    }

    fn create_manager(&self, num_player: PlayerNb) -> Box<dyn RoleManager> {
        self.create_manager(num_player)
    }
}

/// Helper to define a running instance of a role
pub trait RawRoleManager {
    /// Information about the `RoleInfo` associated
    type RoleInfo: RawRoleInfo;

    /// Return if a player should play at a turn
    ///
    /// Default is all player alive that has this role
    fn should_play_at_this_turn(&self, player: &Player, _turn_id: TurnID) -> bool {
        player.alive() && player.has_role(Self::RoleInfo::NAME)
    }

    /// Check if this role has already won
    fn has_won(&self, player_list: &PlayerDb) -> bool;

    /// Handle an event
    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, message::Send)>,
    ) -> TurnInfo;
}

impl<RawImpl: RawRoleManager + Debug> RoleManager for RawImpl {
    fn name(&self) -> RoleID {
        <<Self as RawRoleManager>::RoleInfo as RawRoleInfo>::NAME
    }

    fn should_play_at_this_turn(&self, player: &Player, turn_id: TurnID) -> bool {
        self.should_play_at_this_turn(player, turn_id)
    }

    fn has_won(&self, player_list: &PlayerDb) -> bool {
        self.has_won(player_list)
    }

    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, message::Send)>,
    ) -> TurnInfo {
        self.handle_event(event, player_list, message_sender)
    }
}
