/// Helper traits to define roles
pub mod raw_roles;

/// Trait that define a role
pub mod trait_roles;

/// Helper struct to manage votes
mod vote;
pub use vote::VoteRunner;

/// Information about the number of card distributed for a [`RoleInfo`](trait_roles::RoleInfo)
mod number_info;
pub use number_info::NumberInfo;

pub use raw_roles::{RawRoleInfo, RawRoleManager};
