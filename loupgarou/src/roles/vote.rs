use std::{collections::HashMap, convert::TryInto};

use crate::{consts::PlayerID, message, playerdb::PlayerDb};

use super::trait_roles::{MessageType, TurnInfo};

/// Max number of kind allowed
pub const MAX_KIND: usize = 2;

/// Information about a vite for a player
#[derive(Debug, Default)]
pub struct Vote {
    /// Target voted against for each kind
    to: [Option<PlayerID>; MAX_KIND],

    /// Is this vote finished
    done: bool,
}

/// Informations returned by a `VoteRunner`
pub type Infos = HashMap<(PlayerID, u8), u8>;

/// handle a vote for a turn
#[derive(Debug, Default)]
pub struct VoteRunner {
    /// Vote infos
    vote_info: HashMap<PlayerID, Vote>,
}

impl VoteRunner {
    /// Init this `VoteRunner` for a specific list of players
    pub fn init(&mut self, playing: Vec<PlayerID>) -> TurnInfo {
        self.vote_info = playing
            .into_iter()
            .map(|id| (id, Vote::default()))
            .collect();

        if self.vote_info.is_empty() {
            TurnInfo::TurnDone
        } else {
            TurnInfo::Ok
        }
    }

    /// Return infos about current votes
    #[must_use]
    pub fn infos(&self) -> Infos {
        let mut map = HashMap::new();

        for vote in self.vote_info.values() {
            for (kind, to) in vote
                .to
                .iter()
                .enumerate()
                .flat_map(|(id, vote)| vote.map(|v| (id, v)))
            {
                let kind: u8 = kind.try_into().expect("Kind should fit in an u8");
                *map.entry((to, kind)).or_default() += 1;
            }
        }

        map
    }

    /// Return `true` if all player have voted
    #[must_use]
    pub fn all_player_voted(&self) -> bool {
        self.vote_info.iter().all(|(_, vote)| vote.done)
    }

    /// Return `true` if all player have voted and for each kind <= `kind_nd`
    #[must_use]
    pub fn all_player_voted_each_kind(&self, kind_nb: u8) -> bool {
        let kind_nb: usize = kind_nb.into();

        self.vote_info.iter().all(|(_, vote)| {
            vote.done
                && vote
                    .to
                    .get(..kind_nb)
                    .unwrap_or_default()
                    .iter()
                    .all(Option::is_some)
        })
    }

    /// Kill the player that has most vote
    pub fn kill_most_voted(&self, player_list: &mut PlayerDb) {
        let votes = self.infos();

        if let Some(killed_id) = votes.iter().max_by_key(|&(_, nb)| nb) {
            player_list.kill(killed_id.0 .0);
        }
    }

    /// Handle an event, but return a Result that will be handled by [`handle_event`](Self::handle_event)
    fn _handle_event(
        &mut self,
        from_player: PlayerID,
        msg: &message::Recv,
        _player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, message::Send)>,
    ) -> Result<(), String> {
        let vote = self
            .vote_info
            .get_mut(&from_player)
            .ok_or_else(|| format!("Player {} not allowed to vote", from_player))?;

        match msg {
            message::Recv::VoteFor { player_id, kind } => {
                let kind: usize = (*kind).into();
                if kind >= MAX_KIND {
                    return Err(format!("Kind {} is not valid", kind));
                }

                *vote
                    .to
                    .get_mut(kind)
                    .expect("Kind should be valid (prevously checked)") = *player_id;
            }
            message::Recv::VoteDone(done) => {
                vote.done = *done;
            }
        }

        let infos = self.infos();
        message_sender.push((MessageType::Broadcast, message::Send::Selected(infos)));

        Ok(())
    }

    /// Handle an event
    pub fn handle_event(
        &mut self,
        from_player: PlayerID,
        msg: &message::Recv,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, message::Send)>,
    ) {
        if let Err(err) = self._handle_event(from_player, msg, player_list, message_sender) {
            message_sender.push((MessageType::SendTo(from_player), message::Send::Error(err)));
        }
    }
}
