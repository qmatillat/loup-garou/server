use lp_modeler::dsl::variables::LpInteger;

use crate::consts::PlayerNb;

/// Information about the number of card distributed for a [`RoleInfo`](super::trait_roles::RoleInfo)
pub struct NumberInfo {
    /// Min number for this role
    pub min_nb_card: Option<PlayerNb>,
    /// Max number for this role
    pub max_nb_card: Option<PlayerNb>,
    /// Weight associate. with this role
    pub weight: f32,
}

impl NumberInfo {
    /// Create a new `NumberInfo` with specified weight, and no contrainst.
    #[must_use]
    pub const fn weight(weight: f32) -> Self {
        Self {
            weight,
            max_nb_card: None,
            min_nb_card: None,
        }
    }

    /// Create a new `NumberInfo` with specified weight, and a contrainst to be <= `max`
    #[must_use]
    pub const fn weight_max(weight: f32, max: PlayerNb) -> Self {
        Self {
            weight,
            max_nb_card: Some(max),
            min_nb_card: None,
        }
    }

    /// Create a new `NumberInfo` with specified weight, and a constraint to be present at most one time
    #[must_use]
    pub const fn weight_solo(weight: f32) -> Self {
        Self::weight_max(weight, 1)
    }

    /// Create a new `NumberInfo` with specified weight, and a contrainst to be >= `min`
    #[must_use]
    pub const fn weight_min(weight: f32, min: PlayerNb) -> Self {
        Self {
            weight,
            min_nb_card: Some(min),
            max_nb_card: None,
        }
    }

    /// Create a new `NumberInfo` with specified weight, and a contrainst to be >= 1
    #[must_use]
    pub const fn weight_at_least_one(weight: f32) -> Self {
        Self::weight_min(weight, 1)
    }

    /// Convert this `NumberInfo` into a variable that will be used in the linear solver
    #[must_use]
    pub fn to_variable(&self, name: String) -> LpInteger {
        LpInteger {
            name,
            lower_bound: self.min_nb_card.map(|min| min.into()),
            upper_bound: self.max_nb_card.map(|max| max.into()),
        }
    }
}
