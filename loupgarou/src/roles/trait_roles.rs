use std::fmt::Debug;

use crate::{
    consts::{AlternateRole, PlayerID, PlayerNb, RoleID, TurnID},
    message,
    playerdb::{Player, PlayerDb},
    roles::NumberInfo,
};

/// Static information about a role
pub trait RoleInfo {
    /// Name of this role. Must be unique
    fn name(&self) -> RoleID;

    /// Weight info for this role
    fn default_weight(&self) -> NumberInfo;

    /// Priority of this role (used to determine turn order)
    fn priority(&self) -> u8;

    /// List of alternate role
    fn alternate_role(&self) -> AlternateRole;


    /// Create the manager associated with this role
    fn create_manager(&self, num_player: PlayerNb) -> Box<dyn RoleManager>;
}

/// A running instance of a role
pub trait RoleManager: Debug {
    /// Return the `RoleID` associated with this `RoleInfo`
    fn name(&self) -> RoleID;

    /// Return if a player should play at a turn
    fn should_play_at_this_turn(&self, player: &Player, turn_id: TurnID) -> bool;

    /// Check if this role has already won
    fn has_won(&self, player_list: &PlayerDb) -> bool;

    /// Handle an event
    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, message::Send)>,
    ) -> TurnInfo;
}

/// All event that can be sent to a turn
#[derive(Debug)]
pub enum Event {
    /// Start of this turn
    ///
    /// This contain the list of player that will play at this turn and the turn id.
    StartTurn(Vec<PlayerID>, TurnID),

    /// Message from a player
    MsgPlayer(PlayerID, message::Recv),

    /// On between to step
    ///
    /// This contains the previous turn that just ended, and the next trun that will start
    OnBeetweenStep(RoleID, RoleID),
}

/// Return info for a turn
pub enum TurnInfo {
    /// Turn will continue to run
    Ok,

    /// Turn is over
    TurnDone,
}

/// Information about the destination of a message
#[derive(Debug)]
pub enum MessageType {
    /// Send message to all players
    Broadcast,
    /// Send message to one player
    SendTo(PlayerID),
    /// Send message to all player that have a specific role
    SendToAllOfRole(RoleID),
}
