use std::{
    convert::TryInto,
    iter::{DoubleEndedIterator, ExactSizeIterator, FusedIterator, Iterator},
};

use rand::seq::SliceRandom;

use crate::{
    consts::{AlternateRole, PlayerNb, RoleID},
    roles::trait_roles::RoleInfo,
};

/// Database managing all players
#[derive(Debug)]
pub struct PlayerDb {
    /// List of players
    players: Vec<Player>,
}

impl PlayerDb {
    /// Create a player database based on player names and the role repartition
    #[must_use]
    pub fn create(player_name: Vec<String>, number_role: Vec<(PlayerNb, &dyn RoleInfo)>) -> Self {
        let mut players_role: Vec<_> = number_role
            .into_iter()
            .flat_map(|(nb, role)| vec![role; nb.into()])
            .collect();

        players_role.shuffle(&mut rand::thread_rng());

        let players: Vec<Player> = player_name
            .into_iter()
            .zip(players_role)
            .map(|(name, role)| Player {
                name,
                role: role.name(),
                alternate_role: role.alternate_role(),
                status: PlayerStatus::Alive,
            })
            .collect();

        Self { players }
    }

    /// Return the number of player in this db
    #[must_use]
    pub fn num_player(&self) -> PlayerNb {
        self.players
            .len()
            .try_into()
            .expect("Too many player to fit in PlayerID")
    }

    /// Get info about a player (from `PlayerID`).
    #[must_use]
    pub fn player_info(&self, id: PlayerID) -> Option<&Player> {
        self.players.get(id.to_usize())
    }

    /// Get *mut*able info about a player (from `PlayerID`). (private)
    #[must_use]
    fn player_info_mut(&mut self, id: PlayerID) -> Option<&mut Player> {
        self.players.get_mut(id.to_usize())
    }

    /// Kill a player. This will set the player's status to Dying
    pub fn kill(&mut self, id: PlayerID) -> bool {
        match self.player_info_mut(id) {
            Some(player) if player.alive() => {
                player.status = PlayerStatus::Dying;
                true
            }
            _ => false,
        }
    }

    /// End of a turn, will kill all dying players
    pub fn end_of_turn(&mut self) {
        self.players.iter_mut().for_each(Player::ensure_dead)
    }

    /// Return an iterator over all [`PlayerID`](PlayerID)s
    pub fn ids(
        &self,
    ) -> impl Iterator<Item = PlayerID> + FusedIterator + ExactSizeIterator + DoubleEndedIterator + 'static
    {
        (0..self.num_player()).map(|id| PlayerID::from_usize(id.into()))
    }

    /// Return an iterator over all player
    pub fn list(
        &self,
    ) -> impl Iterator<Item = (PlayerID, &Player)>
           + FusedIterator
           + ExactSizeIterator
           + DoubleEndedIterator {
        self.players
            .iter()
            .enumerate()
            .map(|(id, player)| (PlayerID::from_usize(id), player))
    }

    /// Return an iterator over all player that has the role as main or alternate roles
    pub fn get_of_role(
        &self,
        role: RoleID,
    ) -> impl Iterator<Item = (PlayerID, &Player)> + FusedIterator + DoubleEndedIterator {
        self.list().filter(move |&(_, p)| p.has_role(role))
    }

    /// Return an iterator over all player that has a matching main role
    pub fn get_of_main_role(
        &self,
        role: RoleID,
    ) -> impl Iterator<Item = (PlayerID, &Player)> + FusedIterator + DoubleEndedIterator {
        self.list().filter(move |&(_, p)| p.role == role)
    }

    /// Print all player, in an other format than [`Debug`](fmt::Debug)
    pub fn print(&self) {
        for (id, player) in self.list() {
            println!("{} is a {:?} {}", id, player.status, player.role)
        }
    }
}

/// Status of a player
#[derive(Debug, PartialEq, Eq)]
pub enum PlayerStatus {
    /// Player still alive
    Alive,

    /// Player dying
    Dying,

    /// Player already dead
    Dead,
}

/// Represent a player
#[derive(Debug)]
pub struct Player {
    /// Username of the player
    pub name: String,

    /// Role of this player
    pub role: RoleID,

    /// List of alternate roles for this player
    pub alternate_role: AlternateRole,

    /// Status of the player
    pub status: PlayerStatus,
}

impl Player {
    /// Kill if this player is already dying
    fn ensure_dead(&mut self) {
        if let PlayerStatus::Dying = self.status {
            self.status = PlayerStatus::Dead
        }
    }

    /// Check if a player a specific role (as a primary or secondary role)
    #[must_use]
    pub fn has_role(&self, role: RoleID) -> bool {
        self.role == role || self.alternate_role.contains(&role)
    }

    /// Check if a player is still alive
    #[must_use]
    pub const fn alive(&self) -> bool {
        matches!(self.status, PlayerStatus::Alive)
    }

    /// Check if a player is dying
    ///
    /// A player dyning will die at next turn, but can still be revived
    #[must_use]
    pub const fn dying(&self) -> bool {
        matches!(self.status, PlayerStatus::Dying)
    }

    /// Check if a player is already dead
    #[must_use]
    pub const fn dead(&self) -> bool {
        matches!(self.status, PlayerStatus::Dead)
    }
}

use std::fmt;

/// Identificator for each Player
///
/// Using newtype patern, so that no one can create `PlayerID`
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct PlayerID(u8);

impl PlayerID {
    /// Create a `PlayerID` from an usize.
    ///
    /// This method is private, so only this module can create some `PlayerID`
    fn from_usize(id: usize) -> Self {
        Self(id.try_into().expect("More player than can fit in PlayerID"))
    }

    /// Convert a `PlayerID` back to an usize.
    ///
    /// This method is private, so only this module can use a `PlayerID`
    fn to_usize(self) -> usize {
        self.0.into()
    }
}

impl fmt::Display for PlayerID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Player#{}", self.0)
    }
}
