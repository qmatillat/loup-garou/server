/// Representation of a role
pub type RoleID = &'static str;

/// List of alterante roles
///
/// It's a slice, so it's only allocated statically once
pub type AlternateRole = &'static [RoleID];

/// Turn identifier
pub type TurnID = u16;

/// Represent a number of player
///
/// Currently player number is limited to [`u8::MAX`](std::u8::MAX)
pub type PlayerNb = u8;

pub use crate::playerdb::PlayerID;
