use std::convert::TryInto;

use lp_modeler::solvers::{GlpkSolver, SolverTrait};

use crate::{consts::PlayerNb, roles::trait_roles::RoleInfo};

/// Database of available roles
pub struct RoleDatabase {
    /// List of available roles
    available_roles: Vec<Box<dyn RoleInfo>>,
}

impl Default for RoleDatabase {
    fn default() -> Self {
        Self {
            available_roles: Vec::with_capacity(4),
        }
    }
}

impl RoleDatabase {
    /// Add a role to the database
    pub fn add_role(&mut self, role: impl RoleInfo + 'static) {
        if self.available_roles.iter().any(|r| r.name() == role.name()) {
            panic!("Two roles can't have the same name");
        }

        self.available_roles.push(Box::new(role));
    }

    /// Select roles to be used for a specific number of player
    #[must_use]
    pub fn select(&self, num_player: PlayerNb) -> Vec<(PlayerNb, &dyn RoleInfo)> {
        use lp_modeler::dsl::{LpContinuous, LpExpression, LpObjective, LpOperations, LpProblem};

        let mut problem = LpProblem::new("RoleDistribution", LpObjective::Minimize);
        let mut u = LpContinuous::new("abs");
        u.lower_bound = Some(0.);

        let map = self
            .available_roles
            .iter()
            .map(|role| {
                let w = role.default_weight();
                let role_nb = w.to_variable(role.name().to_string());

                (role, role_nb)
            })
            .collect::<Vec<(_, _)>>();

        problem += map
            .iter()
            .fold(LpExpression::EmptyExpr, |expr, (_, var)| {
                if expr == LpExpression::EmptyExpr {
                    var.into()
                } else {
                    expr + var
                }
            })
            .equal(Into::<i32>::into(num_player));

        let obj = map
            .iter()
            .fold(LpExpression::EmptyExpr, |expr, (role_info, v)| {
                let weighted_var = role_info.default_weight().weight * v;
                if expr == LpExpression::EmptyExpr {
                    weighted_var
                } else {
                    expr + weighted_var
                }
            });

        // Tricks for absolute value
        problem += obj.le(&u);
        problem += (-1 * obj).le(&u);
        problem += u;

        let solver = GlpkSolver::new();
        match solver.run(&problem) {
            Ok(solution) => map
                .into_iter()
                .map(|(role, var)| {
                    (
                        solution
                            .get_int(&var)
                            .try_into()
                            .expect("We have more player than PlayerNB can contain"),
                        &**role,
                    )
                })
                .collect(),
            Err(msg) => panic!("Error while solving : {}", msg),
        }
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::base_roles::{cupidon::CupidonInfo, loups::LoupInfo, villager::VillagerInfo};

    pub(crate) fn roledb() -> RoleDatabase {
        let mut r = RoleDatabase::default();

        r.add_role(LoupInfo);
        r.add_role(VillagerInfo);
        r.add_role(CupidonInfo);

        r
    }

    #[test]
    #[should_panic]
    fn empty_roledb() {
        let _ = RoleDatabase::default().select(5);
    }

    #[test]
    fn test_5() {
        let roledb = roledb();
        let p = roledb.select(5);
        assert_eq!(p.iter().map(|(nb, _)| nb).sum::<u8>(), 5)
    }

    #[test]
    #[should_panic]
    fn test_not_enough_player() {
        let _ = roledb().select(1);
    }
}
