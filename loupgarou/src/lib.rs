//! Runner for Loup-Garou games

#![forbid(
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo,

    // Restrictions
    clippy::as_conversions,
    clippy::clone_on_ref_ptr,
    clippy::create_dir,
    clippy::dbg_macro,
    clippy::decimal_literal_representation,
    clippy::else_if_without_else,
    clippy::exit,
    clippy::filetype_is_file,
    clippy::float_arithmetic,
    clippy::float_cmp_const,
    clippy::get_unwrap,
    clippy::indexing_slicing,
    clippy::let_underscore_must_use,
    clippy::lossy_float_literal,
    clippy::map_err_ignore,
    clippy::mem_forget,
    clippy::missing_docs_in_private_items,
    clippy::modulo_arithmetic,
    clippy::multiple_inherent_impl,
    clippy::panic_in_result_fn,
    clippy::print_stdout,
    clippy::rc_buffer,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::string_add,
    clippy::todo,
    clippy::unimplemented,
    clippy::unneeded_field_pattern,
    clippy::unreachable,
    clippy::unwrap_used,
    clippy::use_debug,
    clippy::verbose_file_reads,
    clippy::wrong_pub_self_convention,
)]
#![warn(clippy::multiple_crate_versions)]
#![allow(clippy::module_name_repetitions)]

/// Role database
mod role_database;
pub use role_database::RoleDatabase;

/// Trait to define roles
pub mod roles;

/// Some base role
pub mod base_roles;


/// Executor that will run a game
mod executor;
pub use executor::Executor;

/// Database of Players
pub mod playerdb;

/// All message type that can be send/received from this server
pub mod message;

/// All constants, and typedef used
pub mod consts;
