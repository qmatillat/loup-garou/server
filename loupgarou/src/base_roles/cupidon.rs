use std::convert::TryInto;

use crate::{
    consts::{AlternateRole, PlayerID, PlayerNb, RoleID, TurnID},
    message::{self, Send},
    playerdb::{Player, PlayerDb},
    roles::{
        trait_roles::{Event, MessageType, RoleManager, TurnInfo},
        NumberInfo, RawRoleInfo, RawRoleManager, VoteRunner,
    },
};

use super::villager::VillagerInfo;

/// Cupidon's role
pub struct CupidonInfo;

/// Manage cupidon
#[derive(Debug)]
enum CupidonManager {
    /// Before vote
    PreVote(VoteRunner),
    /// Lovers pair
    Lovers([PlayerID; 2]),
}

impl RawRoleInfo for CupidonInfo {
    const NAME: RoleID = "Cupidon";
    const WEIGTH: NumberInfo = NumberInfo::weight_solo(0.);
    const PRIORITY: u8 = 0;
    const ALTERNATE_ROLE: AlternateRole = &[VillagerInfo::NAME];

    fn create_manager(&self, num_player: PlayerNb) -> Box<dyn RoleManager> {
        assert_eq!(num_player, 1);

        Box::new(CupidonManager::PreVote(VoteRunner::default()))
    }
}

impl CupidonManager {
    /// Return `VoteRunner` (if available)
    fn vote(&mut self) -> Option<&mut VoteRunner> {
        match self {
            Self::PreVote(vote) => Some(vote),
            Self::Lovers(_) => None,
        }
    }

    /// Return pair of lovers ID (if defined)
    const fn lovers(&self) -> Option<&[PlayerID; 2]> {
        match self {
            Self::PreVote(_) => None,
            Self::Lovers(lovers) => Some(lovers),
        }
    }
}

impl RawRoleManager for CupidonManager {
    type RoleInfo = CupidonInfo;

    fn has_won(&self, player_list: &PlayerDb) -> bool {
        self.lovers().map_or(false, |lovers| {
            player_list
                .list()
                .all(|(id, player)| lovers.contains(&id) ^ player.dead())
        })
    }

    fn should_play_at_this_turn(&self, player: &Player, turn_id: TurnID) -> bool {
        turn_id == 0 && player.role == CupidonInfo::NAME
    }

    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, Send)>,
    ) -> TurnInfo {
        match event {
            Event::StartTurn(playing, 0) => self
                .vote()
                .expect("Cupidon should still be in vote phase at turn 0")
                .init(playing),
            Event::StartTurn(_, _) => TurnInfo::TurnDone,

            Event::MsgPlayer(from, msg) => {
                let vote = self
                    .vote()
                    .expect("Message received after lover has been ellected");
                vote.handle_event(from, &msg, player_list, message_sender);

                if vote.all_player_voted_each_kind(2) {
                    let player = vote
                        .infos()
                        .keys()
                        .map(|(p, _)| p)
                        .cloned()
                        .collect::<Vec<_>>();

                    let player: [PlayerID; 2] = player.try_into().expect("We dont have 2 players");

                    if player[0] == player[1] {
                        message_sender.push((
                            MessageType::Broadcast,
                            message::Send::Error("Can't vote twice to same player".to_string()),
                        ));
                        return TurnInfo::Ok;
                    }

                    *self = Self::Lovers(player);

                    TurnInfo::TurnDone
                } else {
                    TurnInfo::Ok
                }
            }
            Event::OnBeetweenStep(VillagerInfo::NAME, _)
            | Event::OnBeetweenStep(_, VillagerInfo::NAME) => {
                if let Some(p) = self.lovers() {
                    let p1 = player_list
                        .player_info(p[0])
                        .expect("Cupidon Player ID should be valid");
                    let p2 = player_list
                        .player_info(p[1])
                        .expect("Cupidon Player ID should be valid");

                    if !p1.alive() || !p2.alive() {
                        player_list.kill(p[0]);
                        player_list.kill(p[1]);
                    }
                }
                TurnInfo::Ok
            }

            Event::OnBeetweenStep(_, _) => TurnInfo::Ok,
        }
    }
}
