use crate::{
    consts::{AlternateRole, PlayerNb, RoleID},
    message::Send,
    playerdb::PlayerDb,
    roles::{
        trait_roles::{Event, MessageType, RoleManager, TurnInfo},
        NumberInfo, RawRoleInfo, RawRoleManager, VoteRunner,
    },
};

use super::villager::VillagerInfo;

/// Wolf's role
pub struct LoupInfo;

/// Manage Wolfs
#[derive(Debug)]
struct LoupManager {
    /// Information about vote for Wolf
    vote: VoteRunner,
}

impl RawRoleInfo for LoupInfo {
    const NAME: RoleID = "Loups";
    const WEIGTH: NumberInfo = NumberInfo::weight_at_least_one(-100.);
    const PRIORITY: u8 = 5;
    const ALTERNATE_ROLE: AlternateRole = &[VillagerInfo::NAME];

    fn create_manager(&self, _num_player: PlayerNb) -> Box<dyn RoleManager> {
        Box::new(LoupManager {
            vote: VoteRunner::default(),
        })
    }
}

impl RawRoleManager for LoupManager {
    type RoleInfo = LoupInfo;

    fn has_won(&self, player_list: &PlayerDb) -> bool {
        player_list
            .list()
            .all(|(_, player)| player.has_role(LoupInfo::NAME) || player.dead())
    }

    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, Send)>,
    ) -> TurnInfo {
        match event {
            Event::StartTurn(playing, _) => self.vote.init(playing),
            Event::MsgPlayer(from, msg) => {
                self.vote
                    .handle_event(from, &msg, player_list, message_sender);

                if self.vote.all_player_voted() {
                    self.vote.kill_most_voted(player_list);
                    TurnInfo::TurnDone
                } else {
                    TurnInfo::Ok
                }
            }
            Event::OnBeetweenStep(_, _) => TurnInfo::Ok,
        }
    }
}
