use crate::{
    consts::{AlternateRole, PlayerNb, RoleID},
    message::Send,
    playerdb::PlayerDb,
    roles::{
        trait_roles::{Event, MessageType, RoleManager, TurnInfo},
        NumberInfo, RawRoleInfo, RawRoleManager, VoteRunner,
    },
};

/// Villagers' role
pub struct VillagerInfo;

/// Manage villagers
#[derive(Debug)]
struct VillagerManager {
    /// Vote infos
    vote: VoteRunner,
}

impl RawRoleInfo for VillagerInfo {
    const NAME: RoleID = "Villager";
    const WEIGTH: NumberInfo = NumberInfo::weight_at_least_one(50.);
    const PRIORITY: u8 = 200;
    const ALTERNATE_ROLE: AlternateRole = &[];

    fn create_manager(&self, _num_player: PlayerNb) -> Box<dyn RoleManager> {
        Box::new(VillagerManager {
            vote: VoteRunner::default(),
        })
    }
}

impl RawRoleManager for VillagerManager {
    type RoleInfo = VillagerInfo;

    fn has_won(&self, player_list: &PlayerDb) -> bool {
        player_list
            .list()
            .all(|(_, player)| player.has_role(VillagerInfo::NAME) || player.dead())
    }

    fn handle_event(
        &mut self,
        event: Event,
        player_list: &mut PlayerDb,
        message_sender: &mut Vec<(MessageType, Send)>,
    ) -> TurnInfo {
        match event {
            Event::StartTurn(playing, _) => {
                player_list.end_of_turn();
                self.vote.init(playing)
            }
            Event::MsgPlayer(from, msg) => {
                self.vote
                    .handle_event(from, &msg, player_list, message_sender);

                if self.vote.all_player_voted() {
                    self.vote.kill_most_voted(player_list);
                    TurnInfo::TurnDone
                } else {
                    TurnInfo::Ok
                }
            }
            Event::OnBeetweenStep(_, _) => TurnInfo::Ok,
        }
    }
}
