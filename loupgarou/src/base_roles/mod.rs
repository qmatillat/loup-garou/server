/// Cupidon's role
pub mod cupidon;

/// Wolf's role
pub mod loups;

/// Villager's role
pub mod villager;
