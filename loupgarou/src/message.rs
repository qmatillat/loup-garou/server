use std::collections::HashMap;

use crate::{
    consts::{PlayerID, TurnID},
    playerdb::PlayerStatus,
};

/// Message from Clients to Server
#[derive(Debug, PartialEq, Eq)]
pub enum Recv {
    /// Vote for a player with a special kind
    VoteFor {
        /// Player to vote for
        ///
        /// Can be [`None`](Option::None) to remove vote
        player_id: Option<PlayerID>,

        /// Kind of the vote
        kind: u8,
    },

    /// Signal to server that client has finished to vote
    VoteDone(bool),
}

/// Message that can be send by the server
#[derive(Debug, PartialEq, Eq)]
pub enum Send {
    /// Player list information
    ///
    /// Contains the player id, the name and his status
    List(Vec<(PlayerID, String, PlayerStatus)>),

    /// Selection info (used in votes)
    ///
    /// Contains for each player + vote kind, the number of vote
    Selected(HashMap<(PlayerID, u8), u8>),

    /// Return an error
    Error(String),

    /// Start of a turn, with it's name and the turn number
    TurnInfo(String, TurnID),

    /// End of a turn
    EndTurn,
}
